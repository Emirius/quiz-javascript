let quizData = [
  {
    question: 'Which framework is related to JavaScript',
    a: '.net',
    b: 'flask',
    c: 'react',
    d: 'django',
    correct: 'c',
  },
  {
    question: 'Which is not a programming language',
    a: 'html',
    b: 'python',
    c: 'java',
    d: 'JavaScript',
    correct: 'a',
  },
  {
    question: 'Which is not framework',
    a: 'javascript',
    b: 'angular',
    c: 'react',
    d: 'django',
    correct: 'a',
  },
  {
    question: 'CSS stands for ',
    a: 'Cascading style state',
    b: 'Cascading style sheet',
    c: 'Cascading sheet of style',
    d: 'none',
    correct: 'b',
  },
];

let quiz = document.getElementById('quiz');
let answer = document.querySelectorAll('.answer');
let question = document.getElementById('question');
let option_a = document.getElementById('a_value');
let option_b = document.getElementById('b_value');
let option_c = document.getElementById('c_value');
let option_d = document.getElementById('d_value');
let submitBtn = document.getElementById('submit');

let currentQuestion = 0;
let quizScore = 0;

loadQuiz();
function loadQuiz() {
  diselect();
  question.innerHTML = quizData[currentQuestion].question;
  option_a.innerText = quizData[currentQuestion].a;
  option_b.innerText = quizData[currentQuestion].b;
  option_c.innerText = quizData[currentQuestion].c;
  option_d.innerText = quizData[currentQuestion].d;
}

// remove radio button
function diselect() {
  answer.forEach((answer) => (answer.checked = false));
}

submitBtn.addEventListener('click', () => {
  let selectedOption;
  answer.forEach((answer) => {
    if (answer.checked) {
      selectedOption = answer.id;
    }
  });

  if (selectedOption == quizData[currentQuestion].correct) {
    quizScore = quizScore + 1;
  }
  currentQuestion += 1;
  if (currentQuestion == quizData.length) {
    document.getElementById(
      'quizdiv'
    ).innerHTML = `<h1>You have answered ${quizScore} correctly out of ${quizData.length}</h1>`;
  } else {
    loadQuiz();
  }
});
